const c_MinGameSize = 2;
const c_MaxGameSize = 4;
let g_GameSize = 3;
let g_TotalGameSize = g_GameSize ** 2;

let g_BoardMatrix;
let g_BoardMatrixAtStart;

let g_VacantCells = 0;

//////////////////////////////////////////////////////////////////////////////////////////

function generateBoardMatrix() {
    let boardMatrix = new Array(g_TotalGameSize).fill([]);
	for (let i in boardMatrix) {
		boardMatrix[i] = new Array(g_TotalGameSize).fill(0);
	}
	let boardPositions = new Array(g_TotalGameSize ** 2);
	for (let i = 0; i < boardPositions.length;) {
		for (let j = 0; j < g_TotalGameSize; ++j) {
			for (let k = 0; k < g_TotalGameSize; ++k) {
				boardPositions[i] = [j, k];
				i++;
			}
		}
	}
    return { matrix: boardMatrix, positions: boardPositions };
}

function boardMatrixWasModified() {
	for (let i in g_BoardMatrix) {
		for (let j in g_BoardMatrix[i]) {
			if (g_BoardMatrix[i][j] != g_BoardMatrixAtStart[i][j]) {
				return true;
			}
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////

function clearMatrixCellsAccordingToDifficultySetting(difficulty = "easy") {
	if (!(typeof(difficulty) === "string") && !isNaN(difficulty)) {
		g_VacantCells = Math.floor((g_TotalGameSize ** 2) * difficulty);
	} else {
		switch (difficulty) {
			case "debug":
				g_VacantCells = 0;
				break;
			case "cake":
				g_VacantCells = Math.floor((g_TotalGameSize ** 2) * 0.15);
				break;
			case "medium":
				g_VacantCells = Math.floor((g_TotalGameSize ** 2) * 0.5);
				break;
			case "hard":
				g_VacantCells = Math.floor((g_TotalGameSize ** 2) * 0.75);
				break;
			case "insane":
				g_VacantCells = Math.floor((g_TotalGameSize ** 2) * 0.85);
				break;
			default:
				g_VacantCells = Math.floor((g_TotalGameSize ** 2) * 0.25);
		}
	}
	for (let i = 0; i < g_VacantCells;) {
		let randomRow = Utility.getRandomNumInRange(0, g_TotalGameSize - 1);
		let randomPosInRow = Utility.getRandomNumInRange(0, g_TotalGameSize - 1);
		if (g_BoardMatrixAtStart[randomRow][randomPosInRow] != 0) {
			g_BoardMatrixAtStart[randomRow][randomPosInRow] = 0;
			i++;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

function generateRandomBoard(boardMatrix) {
	let row = shuffle(Array.from({ length: g_TotalGameSize }, (value, index) => index + 1));
	for (let i = 0; i < g_TotalGameSize; ++i) {
		if (i > 0) { row = shiftPosition(row, (i % g_GameSize == 0) ? 1 : g_GameSize); }
		boardMatrix[i] = row;
	}
	return boardMatrix;
}

function shuffle(array) {
	for (let i in array) {
		let j = Utility.getRandomNumInRange(0, g_TotalGameSize - 1);
		[array[i], array[j]] = [array[j], array[i]];
	}
	return array;
}

function shiftPosition(array, shiftCount) {
	let outArray = Array.from(array);
	for (let i = 0; i < shiftCount; ++i) {
		outArray.push(outArray.shift());
	}
	return outArray;
}

//////////////////////////////////////////////////////////////////////////////////////////
/*
function generateRandomBoard(matrixData) {
	for (let i = 1; i <= g_TotalGameSize;) {
		matrixData.positionsForNumber = Array.from(matrixData.positions);
		for (let j = 0; j < g_TotalGameSize;) {
			if (placeNumberOnBoardMatrix(matrixData, i)) {
				j++;
			} else {
				i = 1;
				j = 0;
				matrixData = generateBoardMatrix();
				matrixData.positionsForNumber = Array.from(matrixData.positions);
			}
		}
		i++;
	}
	return matrixData.matrix;
}

function placeNumberOnBoardMatrix(matrixData, number, attemptCounter = 0) {
	if (matrixData.positionsForNumber.length == 0) {
		return false;
	}
	let randomPosition = matrixData.positionsForNumber[Utility.getRandomNumInRange(0, matrixData.positionsForNumber.length - 1)];
	let randomRow = randomPosition[0];
	let randomColumn = randomPosition[1];

	if (matrixPositionIsAvailable(matrixData.matrix[randomRow], number)) {
		let columnArray = new Array(g_TotalGameSize);
		for (let row in matrixData.matrix) {
			columnArray[row] = matrixData.matrix[row][randomColumn];
		}
		if (matrixPositionIsAvailable(columnArray, number)) {
			let boxRowStartIndex = 0;
			for (let k = 0; k < g_TotalGameSize; ++k) {
				if (randomRow >= g_GameSize * k && randomRow < g_GameSize * (k + 1)) { boxRowStartIndex = g_GameSize * k; }
			}
			let boxColumnStartIndex = 0;
			for (let k = 0; k < g_TotalGameSize; ++k) {
				if (randomColumn >= g_GameSize * k && randomColumn < g_GameSize * (k + 1)) { boxColumnStartIndex = g_GameSize * k; }
			}
			let boxArray = new Array(g_TotalGameSize);
			for (let y = boxRowStartIndex, indexCounter = 0; y < boxRowStartIndex + g_GameSize; ++y) {
				for (let x = boxColumnStartIndex; x < boxColumnStartIndex + g_GameSize; ++x) {
					boxArray[indexCounter] = matrixData.matrix[y][x];
					indexCounter++;
				}
			}
			if (matrixPositionIsAvailable(boxArray, number)) {
				matrixData.matrix[randomRow][randomColumn] = number;

				matrixData.positionsForNumber = matrixData.positionsForNumber.filter(position => position[0] != randomRow);
				matrixData.positionsForNumber = matrixData.positionsForNumber.filter(position => position[1] != randomColumn);

				matrixData.positions.splice(matrixData.positions.indexOf(randomPosition), 1);
				return true;
			}
		}
	}
	if (attemptCounter > (75 * g_GameSize)) {
		return false;
	}
	matrixData.positionsForNumber.splice(matrixData.positionsForNumber.indexOf(randomPosition), 1);
	return placeNumberOnBoardMatrix(matrixData, number, attemptCounter + 1);
}

function matrixPositionIsAvailable(array, number) {
	return array.indexOf(number) == -1;
}
*/