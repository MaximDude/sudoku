const c_DefaultProfileImageSource = "url(\"assets/default-profile-image.svg\")";

class UserProfile {
    constructor(index, profileName, profileImage = c_DefaultProfileImageSource) {
		this.index = index;
        this.profileName = profileName;
		this.profileImage = profileImage;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

class ProfileManager {
	static initialize() {
		this.creationDialogProfileIndex = -1;
		this.activeProfile = null;
		this.profiles = new Array(10);
		let loadedProfiles = window.localStorage.getItem("sudokuProfiles");
		if (loadedProfiles) { this.profiles = JSON.parse(loadedProfiles); }
	}

	static createProfile(profileName, profileImageSource) {
		let newUserProfile = new UserProfile(this.creationDialogProfileIndex, profileName, profileImageSource);
		this.profiles[this.creationDialogProfileIndex] = newUserProfile;
		this.creationDialogProfileIndex = -1;

		window.localStorage.setItem("sudokuProfiles", JSON.stringify(this.profiles));

		return newUserProfile;
	}

	static getProfileByIndex(profileIndex) {
		return (this.profiles[profileIndex] != undefined) ? this.profiles[profileIndex] : null;
	}

	static getAllProfiles() {
		return this.profiles;
	}

	static getActiveProfile() {
		return this.activeProfile;
	}

	static setActiveProfile(profileIndex) {
		this.activeProfile = this.profiles[profileIndex];
	}

	static setCreationDialogProfileIndex(profileIndex = -1) {
		this.creationDialogProfileIndex = profileIndex;
	}

	static printStoredProfiles() {
		console.log(JSON.parse(window.localStorage.getItem("sudokuProfiles")));
	}

	static clearAllProfiles() {
		window.localStorage.removeItem("sudokuProfiles");
	}
}