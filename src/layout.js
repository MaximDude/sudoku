
const c_Screens = new Map([
	["WelcomeScreen", document.getElementById("welcome-screen")],
	["GameSettingsScreen", document.getElementById("game-settings-screen")],
	["BoardScreen", document.getElementById("board-screen")]
]);

const c_GameHeader = document.getElementById("game-header");
const c_PromptOverlay = document.getElementById("prompt-overlay");
const c_ProfileCreatePrompt = document.getElementById("profile-create-prompt-window");
const c_PromptButtonContainers = new Map();
const c_WelcomeScreenProfileButtons = new Map();

let g_UserWelcomeMessage = null;
let g_ProfileImage = null;
let g_ProfileCreateProfileImage = null;
let g_ProfileBannerProfileImage = null;
let g_WindowCloseButton = null;
let g_PromptWindow = null;
let g_BoardContainer = null;
let g_BoardControlsContainer = null;
let g_BoardInputControlsContainer = null;

//////////////////////////////////////////////////////////////////////////////////////////

function buildLayout() {
	createSharedElements();

    createGameHeader();
	createWelcomeScreen();
    createGameSettingsScreen();
    createBoardScreen();
    createPromptWindow();

	createProfileCreationPromptWindow();
}

//////////////////////////////////////////////////////////////////////////////////////////

function createSharedElements() {
	g_WindowCloseButton = document.createElement("button");
	g_WindowCloseButton.classList.add("window-close-button");
	g_WindowCloseButton.classList.add("painted-red");
	g_WindowCloseButton.style.backgroundImage = "url(\"assets/close-button-icon.svg\")";

	g_ProfileImage = document.createElement("div");
	g_ProfileImage.classList.add("profile-image");
	g_ProfileImage.style.backgroundImage = c_DefaultProfileImageSource;
}

//////////////////////////////////////////////////////////////////////////////////////////

function createGameHeader() {
	let gameTitle = Utility.createElementWithId("h1", "game-title");
	c_GameHeader.appendChild(gameTitle);

	gameTitle.innerHTML = "SUDOKU";
}

//////////////////////////////////////////////////////////////////////////////////////////

function createWelcomeScreen() {
	let welcomeScreenContainer = Utility.createElementWithId("div", "welcome-screen-container");
	c_Screens.get("WelcomeScreen").appendChild(welcomeScreenContainer);

	let welcomeMessage = Utility.createElementWithId("h2", "welcome-message");
	welcomeMessage.innerHTML = "Welcome to Sudoku!";
	welcomeScreenContainer.appendChild(welcomeMessage);

	let profileMessage = Utility.createElementWithId("h3", "welcome-profile-info-message");
	profileMessage.innerHTML = "Please choose and existing profile or create a new one by selecting an empty profile";
	welcomeScreenContainer.appendChild(profileMessage);

	let profileButtonContainer = Utility.createElementWithId("div", "welcome-screen-button-container");
	welcomeScreenContainer.appendChild(profileButtonContainer);

	for (let i = 0; i < 10; ++i) {
		let profileButton = document.createElement("button");
		profileButton.setAttribute("value", i);
		profileButton.classList.add("welcome-screen-profile-button");
		profileButtonContainer.appendChild(profileButton);
		c_WelcomeScreenProfileButtons.set(profileButton, null);
	}
	createWelcomeScreenProfileButtonHandlers();
}

function createWelcomeScreenProfileButtonHandlers() {
	for (let profileButton of c_WelcomeScreenProfileButtons.keys()) {
		let userProfile = ProfileManager.getProfileByIndex(profileButton.value);

		let existingProfileButtonHandler = () => { selectProfileAndProceed(userProfile); }
		let emptyProfileButtonHandler = () => { ProfileManager.setCreationDialogProfileIndex(profileButton.value); showOrHideProfileCreationDialog(true); };

		if (userProfile) {
			profileButton.innerHTML = userProfile.profileName;

			c_WelcomeScreenProfileButtons.set(profileButton, existingProfileButtonHandler);
			profileButton.addEventListener("click", existingProfileButtonHandler);
		} else {
			profileButton.classList.add("empty-profile");
			profileButton.innerHTML = "Empty";

			c_WelcomeScreenProfileButtons.set(profileButton, emptyProfileButtonHandler);
			profileButton.addEventListener("click", emptyProfileButtonHandler);
		}
	}
}

function recreateWelcomeScreenProfileButtonHandlers() {
	for (let [profileButton, buttonEventListener] of c_WelcomeScreenProfileButtons) {
		profileButton.classList.remove("empty-profile");
		profileButton.removeEventListener("click", buttonEventListener);
	}
	createWelcomeScreenProfileButtonHandlers();
}

//////////////////////////////////////////////////////////////////////////////////////////

function createGameSettingsScreen() {
	let gameSettingsScreenContainer = Utility.createElementWithId("div", "game-settings-screen-container")
	c_Screens.get("GameSettingsScreen").appendChild(gameSettingsScreenContainer);

	createProfileBanner(gameSettingsScreenContainer);

	let gameSettingsControlsContainer = Utility.createElementWithId("div", "game-settings-container");
	gameSettingsScreenContainer.appendChild(gameSettingsControlsContainer);

	let simpleGameControlsContainer = Utility.createElementWithId("div", "game-settings-controls-container");
	simpleGameControlsContainer.classList.add("visible");
	createSimpleGameSettingControls(simpleGameControlsContainer);

	let advancedGameControlsContainer = Utility.createElementWithId("div", "game-settings-controls-container");
	createAdvancedGameSettingControls(advancedGameControlsContainer);

	let toggleSettingsModeButton = Utility.createElementWithId("button", "game-settings-toggle-button");
	toggleSettingsModeButton.classList.add("control-button");
	toggleSettingsModeButton.innerHTML = "Classic Game";
	gameSettingsControlsContainer.appendChild(toggleSettingsModeButton);
	gameSettingsControlsContainer.appendChild(simpleGameControlsContainer);
	gameSettingsControlsContainer.appendChild(advancedGameControlsContainer);

	toggleSettingsModeButton.addEventListener("click", () => {
		toggleSettingsModeButton.innerHTML = (toggleSettingsModeButton.innerHTML == "Classic Game") ? "Custom Game" : "Classic Game";
		simpleGameControlsContainer.classList.toggle("visible");
		advancedGameControlsContainer.classList.toggle("visible");
	})
}

function createProfileBanner(parentContainer) {
	let profileBannerOuterContainer = Utility.createElementWithId("div", "profile-banner-outer-container");
	parentContainer.appendChild(profileBannerOuterContainer);

	g_ProfileBannerProfileImage = g_ProfileImage.cloneNode(true);
	profileBannerOuterContainer.appendChild(g_ProfileBannerProfileImage);

	let profileBannerInnerContainer = Utility.createElementWithId("div", "profile-banner-inner-container");
	profileBannerOuterContainer.appendChild(profileBannerInnerContainer);

	g_UserWelcomeMessage = Utility.createElementWithId("p", "profile-banner-welcome-message");
	g_UserWelcomeMessage.innerHTML = "Hello, user!"
	profileBannerInnerContainer.appendChild(g_UserWelcomeMessage);

	let profileControlsContainer = Utility.createElementWithId("div", "profile-controls-container");
	profileBannerInnerContainer.appendChild(profileControlsContainer);

	let changeProfileButton = Utility.createElementWithId("button", `button-change-profile`);
	changeProfileButton.classList.add("profile-banner-control-button");
	profileControlsContainer.appendChild(changeProfileButton);

	changeProfileButton.innerHTML = "Change Profile";

	changeProfileButton.addEventListener("click", changeProfile);
}

function createSimpleGameSettingControls(parentContainer) {
	let difficultyButtons = ["cake", "easy", "medium", "hard", "insane"];

	for (let difficultyButtonName of difficultyButtons) {
		let difficultyButton = document.createElement("button");
		difficultyButton.classList.add("control-button");
		difficultyButton.classList.add("larger-font");
		difficultyButton.classList.add("wider");
		parentContainer.appendChild(difficultyButton);

		difficultyButton.innerHTML = difficultyButtonName;

		difficultyButton.addEventListener("click", () => { startGame(difficultyButton.innerHTML); showScreen("BoardScreen"); });
	}
}

function createAdvancedGameSettingControls(parentContainer) {
	let gameSizeSliderContainer = document.createElement("div");
	gameSizeSliderContainer.classList.add("game-settings-slider-container");
	parentContainer.appendChild(gameSizeSliderContainer);

	let gameSizeSliderLabel = document.createElement("h2");
	gameSizeSliderContainer.appendChild(gameSizeSliderLabel);

	gameSizeSliderLabel.innerHTML = "Game Size";

	let gameSizeSlider = Utility.createElementWithId("input", "slider-game-size");
	gameSizeSlider.setAttribute("type", "range");
	gameSizeSlider.setAttribute("min", c_MinGameSize);
	gameSizeSlider.setAttribute("max", c_MaxGameSize);
	gameSizeSlider.setAttribute("value", 3);
	gameSizeSliderContainer.appendChild(gameSizeSlider);

	let gameDifficultySliderContainer = document.createElement("div");
	gameDifficultySliderContainer.classList.add("game-settings-slider-container");
	parentContainer.appendChild(gameDifficultySliderContainer);

	let gameDifficultySliderLabel = document.createElement("h2");
	gameDifficultySliderContainer.appendChild(gameDifficultySliderLabel);

	gameDifficultySliderLabel.innerHTML = "Game Difficulty";

	let gameDifficultySlider = Utility.createElementWithId("input", "slider-game-difficulty");
	gameDifficultySlider.setAttribute("type", "range");
	gameDifficultySlider.setAttribute("min", 10);
	gameDifficultySlider.setAttribute("max", 90);
	gameDifficultySlider.setAttribute("value", 50);
	gameDifficultySliderContainer.appendChild(gameDifficultySlider);

	let gameStartButton = document.createElement("button");
	gameStartButton.classList.add("control-button");
	parentContainer.appendChild(gameStartButton);

	gameStartButton.innerHTML = "Start Game";

	gameStartButton.addEventListener("click", () => { setGameSize(parseInt(gameSizeSlider.value)); startGame(parseFloat(gameDifficultySlider.value) / 100); showScreen("BoardScreen"); });
}

//////////////////////////////////////////////////////////////////////////////////////////

function createBoardScreen() {
	createBoardContainer();
	mapBoardCellCollections();
	Utility.executeNonBlocking(createGameControls);
}

function recreateBoardScreen() {
	Utility.removeAllElementsFromContainer(c_Screens.get("BoardScreen"));
	clearBoardCellCollections();
	createBoardScreen();
}

function createBoardContainer() {
	g_BoardContainer = Utility.createElementWithId("div", "board-container");

	g_BoardContainer.style.gridTemplateRows = `repeat(${g_GameSize}, 1fr)`;
	g_BoardContainer.style.gridTemplateColumns = `repeat(${g_GameSize}, 1fr)`;

	c_Screens.get("BoardScreen").appendChild(g_BoardContainer);
	createBoardBoxes();
}

function createBoardBoxes() {
	for (let y = 0; y < g_GameSize; ++y) {
		for (let x = 0; x < g_GameSize; ++x) {
			let boardBox = Utility.createElementWithId("div", `box-${y}-${x}`);
			boardBox.classList.add("board-box");

			boardBox.style.gridTemplateRows = `repeat(${g_GameSize}, 1fr)`;
			boardBox.style.gridTemplateColumns = `repeat(${g_GameSize}, 1fr)`;

			g_BoardContainer.appendChild(boardBox);

			createBoardBoxCells(boardBox);
			c_BoardBoxes.set(boardBox.id, Array.from(boardBox.children));
		}
	}
}

function createBoardBoxCells(boardBox) {
	for (let y = 0; y < g_GameSize; ++y) {
		for (let x = 0; x < g_GameSize; ++x) {
			let boardCell = Utility.createElementWithId("button", `${boardBox.id}-cell-${y}-${x}`);
			boardCell.classList.add("board-cell");

			boardCell.addEventListener("mousedown", (event) => { Utility.executeNonBlocking(selectBoardCell, event.target); });
			boardCell.addEventListener("keydown", (keyboardEvent) => { Utility.executeNonBlocking(applyNumberValueToSelectedCellViaKeyboard, keyboardEvent); });

			boardBox.appendChild(boardCell);
			c_BoardCells.set(boardCell.id, boardCell);
		}
	}
}

function createGameControls() {
	g_BoardControlsContainer = Utility.createElementWithId("div", "board-controls-container");
	c_Screens.get("BoardScreen").appendChild(g_BoardControlsContainer);

	let controlsContainer = Utility.createElementWithId("div", "board-control-button-container");
	g_BoardControlsContainer.appendChild(controlsContainer);

	Utility.executeNonBlocking(createGameInputControls);

	let controlButtons = [
		["check", checkBoard],
		["clear-board", clearAllCells],
		["shuffle", () => { shuffleBoard(); }],
		["change-difficulty", () => { deselectBoardCell(); Utility.executeNonBlocking(paintInvalidBoardCells); showScreen("GameSettingsScreen"); }]
	];

	for (let [buttonName, buttonFunction] of controlButtons) {
		let controlButton = Utility.createElementWithId("button", `board-button-${buttonName}`);
		controlButton.classList.add("control-button");
		controlButton.innerHTML = buttonName.replace('-', ' ');

		controlButton.addEventListener("click", buttonFunction);

		controlsContainer.appendChild(controlButton);
	}
}

function createGameInputControls() {
	g_BoardInputControlsContainer = Utility.createElementWithId("div", "board-input-controls-container");
	g_BoardControlsContainer.appendChild(g_BoardInputControlsContainer);

	let numberButtonContainer = document.createElement("div");
	numberButtonContainer.style.width = "100%";
	g_BoardInputControlsContainer.appendChild(numberButtonContainer);

	Utility.executeNonBlocking(createGameInputExtraControls);

	for (let i = 0; i < g_TotalGameSize; ++i) {
		let numberButton = document.createElement("button");
		numberButton.classList.add("board-number-input-button");
		numberButton.setAttribute("value", i + 1);
		numberButton.innerHTML = numberButton.value;

		numberButton.addEventListener("click", () => { Utility.executeNonBlocking(applyNumberValueToSelectedCell, numberButton.value); });

		numberButtonContainer.appendChild(numberButton);
	}
}

function createGameInputExtraControls() {
	let inputButtonContainer = Utility.createElementWithId("div", "board-input-controls-bottom-row");
	g_BoardInputControlsContainer.appendChild(inputButtonContainer);

	let inputButtons = [["clear", clearSelectedCell], ["close", deselectBoardCell]];

	for (let [buttonName, buttonFunction] of inputButtons) {
		let inputButton = Utility.createElementWithId("button", `board-button-${buttonName}`);
		inputButton.classList.add("control-button");
		inputButton.classList.add("wider");

		inputButton.innerHTML = buttonName;

		inputButton.addEventListener("click", buttonFunction);

		inputButtonContainer.appendChild(inputButton);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

function createPromptWindow() {
	g_PromptWindow = Utility.createElementWithId("dialog", "prompt-window");
	c_PromptOverlay.appendChild(g_PromptWindow);

	let promptTitle = document.createElement("h2");
	promptTitle.classList.add("prompt-title");
	g_PromptWindow.appendChild(promptTitle);

	let promptMessage = Utility.createElementWithId("p", "prompt-message");
	g_PromptWindow.appendChild(promptMessage);

	let promptForm = document.createElement("form");
	promptForm.setAttribute("method", "dialog");
	g_PromptWindow.appendChild(promptForm);

	let buttonContainers = [
		["confirm", [["OK", false]]],
		["dialog", [["yes", true], ["no", false]]],
		["victory", [["new game", () => { shuffleBoard(true); }], ["change difficulty", () => { showScreen("GameSettingsScreen"); }]]]
	];

	for (let [containerName, containerButtons] of buttonContainers) {
		let buttonContainer = document.createElement("menu");
		c_PromptButtonContainers.set(containerName, buttonContainer);
		promptForm.appendChild(buttonContainer);

		for (let [buttonText, buttonValueOrFunction] of containerButtons) {
			let containerButton = document.createElement("button");
			containerButton.classList.add("control-button");
			containerButton.classList.add("wider");
			containerButton.classList.add("larger-font");
			buttonContainer.appendChild(containerButton);

			containerButton.innerHTML = buttonText;

			if (typeof(buttonValueOrFunction) == "function") {
				containerButton.addEventListener("click", buttonValueOrFunction);
			} else {
				containerButton.value = buttonValueOrFunction;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

function createProfileCreationPromptWindow() {
	let profileCreateWindowCloseButton = g_WindowCloseButton.cloneNode(true);
	c_ProfileCreatePrompt.appendChild(profileCreateWindowCloseButton);

	let profileCreateTitle = document.createElement("h2");
	profileCreateTitle.classList.add("prompt-title");
	profileCreateTitle.innerHTML = "Create New Profile";
	c_ProfileCreatePrompt.appendChild(profileCreateTitle);

	let profileCreateForm = Utility.createElementWithId("form", "profile-create-input-form");
	c_ProfileCreatePrompt.appendChild(profileCreateForm);

	createProfileCreationForm(profileCreateForm);

	profileCreateWindowCloseButton.addEventListener("click", () => { showOrHideProfileCreationDialog(); });
}

function createProfileCreationForm(parentContainer) {
	let profileImageContainer = Utility.createElementWithId("div", "profile-image-container");
	parentContainer.appendChild(profileImageContainer);

	g_ProfileCreateProfileImage = g_ProfileImage.cloneNode(true);
	profileImageContainer.appendChild(g_ProfileCreateProfileImage);

	let imageUploadHandler = document.createElement("input");
	imageUploadHandler.setAttribute("type", "file");
	imageUploadHandler.setAttribute("accept", "image/*");
	profileImageContainer.appendChild(imageUploadHandler);

	g_ProfileCreateProfileImage.addEventListener("click", () => { imageUploadHandler.click(); });

	imageUploadHandler.addEventListener("change", async () => {
		let imageData = await Utility.getFileAsBase64(imageUploadHandler.files[0]);
		g_ProfileCreateProfileImage.style.backgroundImage = `url("${imageData}")`;
	});

	let uploadImageButton = Utility.createElementWithId("button", "profile-image-upload-button");
	uploadImageButton.setAttribute("type", "button");
	profileImageContainer.appendChild(uploadImageButton);

	uploadImageButton.innerHTML = "Upload Image";

	uploadImageButton.addEventListener("click", () => { imageUploadHandler.click(); });

	let profileNameContainer = Utility.createElementWithId("div", "profile-name-container");
	parentContainer.appendChild(profileNameContainer);

	let profileNameLabel = Utility.createElementWithId("h3", "profile-name-textbox-label");
	profileNameContainer.appendChild(profileNameLabel);

	profileNameLabel.innerHTML = "Profile Name:";

	let profileNameInput = Utility.createElementWithId("input", "profile-name-textbox");
	profileNameInput.setAttribute("type", "text");
	profileNameInput.setAttribute("minlength", "2");
	profileNameInput.setAttribute("maxlength", "15");
	profileNameInput.setAttribute("placeholder", "Enter profile name...");
	profileNameInput.setAttribute("required", '');
	profileNameContainer.appendChild(profileNameInput);

	let createProfileButton = Utility.createElementWithId("button", "profile-create-button");
	createProfileButton.setAttribute("type", "submit");
	createProfileButton.classList.add("control-button");
	profileNameContainer.appendChild(createProfileButton);

	createProfileButton.innerHTML = "Create Profile";

	parentContainer.addEventListener("submit", (event) => {
		event.preventDefault();
		let newUserProfile = ProfileManager.createProfile(profileNameInput.value, g_ProfileCreateProfileImage.style.backgroundImage);
		if (newUserProfile) {
			selectProfileAndProceed(newUserProfile);
			showOrHideProfileCreationDialog();

			Utility.executeNonBlocking(recreateWelcomeScreenProfileButtonHandlers);
		}
	});
}