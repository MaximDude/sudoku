ProfileManager.initialize();

document.addEventListener("DOMContentLoaded", () => {
	buildLayout();
});

window.onload = () => {
	showScreen("WelcomeScreen");
}

window.onbeforeunload = () => {
	return '';
}

//////////////////////////////////////////////////////////////////////////////////////////

function showScreen(screenToShow) {
	for (let screen of c_Screens.values()) {
		screen.style.display = "none";
	}
	c_Screens.get(screenToShow).style.display = "flex";
}

async function showPrompt(promptTitle, promptMessage, promptType = "dialog") {
	g_PromptWindow.children[0].innerHTML = promptTitle;
	g_PromptWindow.children[1].innerHTML = promptMessage;

	for (let buttonContainer of c_PromptButtonContainers.values()) {
		buttonContainer.style.display = "none";
	}
	c_PromptButtonContainers.get(promptType).style.display = "flex";

	c_PromptOverlay.classList.add("visible");
	g_PromptWindow.classList.add("visible");
	g_PromptWindow.showModal();

	let promptResult = new Promise((resolve, reject) => {
		g_PromptWindow.addEventListener("close", () => {
			c_PromptOverlay.classList.remove("visible");
			g_PromptWindow.classList.remove("visible");

			let result = g_PromptWindow.returnValue;
			if (result != undefined) {
				resolve(result == "true");
			} else {
				reject();
			}
		}, { once: true });
	});
	return await promptResult;
}

function selectProfileAndProceed(selectedProfile) {
    ProfileManager.setActiveProfile(selectedProfile.index);
    g_UserWelcomeMessage.innerHTML = `Hello, ${selectedProfile.profileName}!`;
	g_ProfileBannerProfileImage.style.backgroundImage = selectedProfile.profileImage;
    showScreen("GameSettingsScreen");
}

function changeProfile() {
	ProfileManager.setActiveProfile(-1);
	showScreen("WelcomeScreen");
}

function showOrHideProfileCreationDialog(show = false) {
	if (show) {
		c_PromptOverlay.classList.add("visible");
		c_ProfileCreatePrompt.classList.add("visible");
	} else {
		c_PromptOverlay.classList.remove("visible");
		c_ProfileCreatePrompt.classList.remove("visible");
		document.getElementById("profile-name-textbox").value = '';
		g_ProfileCreateProfileImage.style.backgroundImage = c_DefaultProfileImageSource;
		ProfileManager.setCreationDialogProfileIndex();
	}
}