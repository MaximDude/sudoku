class Utility {

	static executeNonBlocking(functionPointer, ...args) {
		setTimeout(() => { functionPointer(...args); }, 0);
	}

//////////////////////////////////////////////////////////////////////////////////////////

	static createElementWithId(elementType, identifier) {
		if ((typeof(identifier) === "string" || (identifier instanceof String)) && identifier.length >= 1) {
			let newElement = document.createElement(elementType);
			newElement.id = identifier;
			return newElement;
		} else {
			throw new Error("invalid or empty identifier specified");
		}
	}

	static removeAllElementsFromContainer(container) {
		while (container.firstChild) {
			container.removeChild(container.firstChild);
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////

	static getRandomNumInRange(min, max) {
		min = Math.floor(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1) + min);
	}

//////////////////////////////////////////////////////////////////////////////////////////

	static logMatrix(matrix) {
		for (let i of matrix) {
			console.log(i.toString());
		}
	}

	static cloneMatrix(matrix) {
		let newMatrix = new Array(matrix.length).fill([]);
		for (let i in matrix) {
			newMatrix[i] = Array.from(matrix[i]);
		}
		return newMatrix;
	}

//////////////////////////////////////////////////////////////////////////////////////////

	static getFileAsBase64(file) {
		return new Promise((resolve, reject) => {
			let fileReader = new FileReader();

			fileReader.onload = async () => {
				try {
					let base64Data = await fileReader.result;
					base64Data = base64Data.replace(/[\s\n\r]+/, '');
					resolve(base64Data);
				} catch (error) {
					reject(error);
				}
			}
			fileReader.onerror = (error) => { reject(error); }

			fileReader.readAsDataURL(file);
		});
	}
}