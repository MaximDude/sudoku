const c_BoardCells = new Map();
const c_BoardBoxes = new Map();
const c_BoardRows = new Map();
const c_BoardColumns = new Map();

let g_DifficultySetting = "easy";

let g_InvalidBoardCells = new Set();
let g_SelectedBoardCell = null;

//////////////////////////////////////////////////////////////////////////////////////////

function startGame(difficultySetting = g_DifficultySetting) {
	g_DifficultySetting = difficultySetting;

	clearAndUnlockBoardCells();
	g_BoardMatrixAtStart = generateRandomBoard(generateBoardMatrix().matrix);
	clearMatrixCellsAccordingToDifficultySetting(g_DifficultySetting);
	g_BoardMatrix = Utility.cloneMatrix(g_BoardMatrixAtStart);
	mapBoardCellsToMatrix();
	Utility.executeNonBlocking(fillBoardCellsWithMatrixDataAndLockIfOccupied);
}

async function shuffleBoard(skipDiff = false) {
	deselectBoardCell();
	paintInvalidBoardCells();

	let promptResult = (!skipDiff && boardMatrixWasModified()) ? await showPrompt("Current progress will be lost!", "Start a new game anyway?", "dialog") : true;
	if (promptResult == true) { startGame(); }
}

function setGameSize(gameSize = 3) {
	if (gameSize < c_MinGameSize || gameSize > c_MaxGameSize) { gameSize = 3; }
	g_GameSize = gameSize;
	g_TotalGameSize = g_GameSize ** 2;
	recreateBoardScreen();
}

//////////////////////////////////////////////////////////////////////////////////////////

function clearBoardCellCollections() {
	c_BoardCells.clear();
	c_BoardBoxes.clear();
	c_BoardRows.clear();
	c_BoardColumns.clear();
}

function mapBoardCellCollections() {
	let boardCellCollections = [["row", c_BoardRows], ["column", c_BoardColumns]];

	// commence hacky garbage
	for (let [collectionIdentifier, cellCollection] of boardCellCollections) {
		let loopAxis = ['Y', 'X'];
		if (collectionIdentifier == "column") { [loopAxis[0], loopAxis[1]] = [loopAxis[1], loopAxis[0]]; }

		let mappingFunction = new Function("identifier", "collection", `
			let cellArrayCounter = 0;

			for (let box${loopAxis[0]} = 0; box${loopAxis[0]} < g_GameSize; ++box${loopAxis[0]}) {
				for (let cell${loopAxis[0]} = 0; cell${loopAxis[0]} < g_GameSize; ++cell${loopAxis[0]}) {
					let cellArray = new Array(g_TotalGameSize);
					let posInCellArray = 0;

					for (let box${loopAxis[1]} = 0; box${loopAxis[1]} < g_GameSize; ++box${loopAxis[1]}) {
						let boxIdentifier = "box-" + boxY + "-" + boxX;

						for (let cell${loopAxis[1]} = 0; cell${loopAxis[1]} < g_GameSize; ++cell${loopAxis[1]}) {
							let boardCell = c_BoardCells.get(boxIdentifier + "-cell-" + cellY + "-" + cellX);
							boardCell.classList.add("board-" + identifier + "-" + cellArrayCounter);
							cellArray[posInCellArray] = boardCell;
							posInCellArray++;
						}
					}
					collection.set("board-" + identifier + "-" + cellArrayCounter, cellArray);
					cellArrayCounter++;
				}
			}
		`);
		mappingFunction(collectionIdentifier, cellCollection);
	}
}

function mapBoardCellsToMatrix() {
	let row = 0;
	let posInRow = 0;

	for (let boardRow of c_BoardRows.values()) {
		for (let boardCell of boardRow) {
			boardCell.setAttribute("value", `${row}-${posInRow}`);
			posInRow++;
		}
		if (posInRow >= g_BoardMatrix.length) {
			posInRow = 0;
			row++;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

function clearAndUnlockBoardCells() {
	for (let boardCell of c_BoardCells.values()) {
		boardCell.innerHTML = '';
		boardCell.removeAttribute("disabled");
	}
}

async function clearAllCells() {
	Utility.executeNonBlocking(paintInvalidBoardCells);
	deselectBoardCell();

	let promptResult = boardMatrixWasModified() ? await showPrompt("Current progress will be lost!", "Clear board anyway?", "dialog") : true;
	if (promptResult == true) {
		for (let boardCell of c_BoardCells.values()) {
			if (!boardCell.disabled) { boardCell.innerHTML = ''; }
		}
		g_BoardMatrix = Utility.cloneMatrix(g_BoardMatrixAtStart);
	}
}

function fillBoardCellsWithMatrixDataAndLockIfOccupied() {
	let row = 0;
	let posInRow = 0;

	for (let boardRow of c_BoardRows.values()) {
		for (let boardCell of boardRow) {
			let matrixValue = g_BoardMatrix[row][posInRow];
			if (matrixValue == 0) {
				boardCell.innerHTML = '';
			} else {
				boardCell.innerHTML = matrixValue;
				boardCell.setAttribute("disabled", '');
			}
			posInRow++

			if (posInRow > boardRow.length - 1) {
				posInRow = 0;
				row++;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

function applyNumberValueToSelectedCell(numberValue) {
	if (g_SelectedBoardCell) {
		g_SelectedBoardCell.innerHTML = numberValue;
		updateSelectedCellValueInBoardMatrix(numberValue);
		deselectBoardCell();
	}
}

function applyNumberValueToSelectedCellViaKeyboard(keyboardEvent) {
	if (document.activeElement.id == keyboardEvent.target.id) {
		if (keyboardEvent.key == "Escape") {
			deselectBoardCell();
		} else if (keyboardEvent.key == "Delete" || keyboardEvent.key == "Backspace") {
			clearSelectedCell();
		} else if ((keyboardEvent.key >= 1 && keyboardEvent.key <= 9)) {
			applyNumberValueToSelectedCell(keyboardEvent.key);
		}
	}
}

function clearSelectedCell() {
	if (g_SelectedBoardCell) {
		if (g_SelectedBoardCell.innerHTML == '') {
			deselectBoardCell();
		} else {
			g_SelectedBoardCell.innerHTML = '';
			updateSelectedCellValueInBoardMatrix();
		}
	}
}

function updateSelectedCellValueInBoardMatrix(cellValue = 0) {
	let cellIndex = g_SelectedBoardCell.value.split('-');
	g_BoardMatrix[cellIndex[0]][cellIndex[1]] = parseInt(cellValue);
}

//////////////////////////////////////////////////////////////////////////////////////////

function selectBoardCell(boardCell) {
	Utility.executeNonBlocking(paintInvalidBoardCells);

	if (g_SelectedBoardCell == boardCell) {
		deselectBoardCell();
	} else {
		if (g_SelectedBoardCell != null) {
			g_SelectedBoardCell.classList.remove("focused");
			Utility.executeNonBlocking(paintBoardCellsRelatedToSelectedCell, g_SelectedBoardCell);
		}
		g_SelectedBoardCell = boardCell;
		g_SelectedBoardCell.classList.add("focused");
		Utility.executeNonBlocking(showOrHideGameInputControls, true);
		Utility.executeNonBlocking(paintBoardCellsRelatedToSelectedCell, g_SelectedBoardCell, true);
	}
}

function deselectBoardCell() {
	if (g_SelectedBoardCell) {
		Utility.executeNonBlocking(paintBoardCellsRelatedToSelectedCell, g_SelectedBoardCell);
		Utility.executeNonBlocking(showOrHideGameInputControls);
		g_SelectedBoardCell.classList.remove("focused");
		g_SelectedBoardCell = null;
	}
}

function showOrHideGameInputControls(show = false) {
	if (show) {
		g_BoardInputControlsContainer.classList.add("visible");
	} else {
		g_BoardInputControlsContainer.classList.remove("visible");
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

function paintBoardCellsRelatedToSelectedCell(selectedCell, paint = false) {
	let cellBoxCells = c_BoardBoxes.get(selectedCell.id.split("-cell")[0]);
	let cellRowCells = [];
	let cellColumnCells = [];

	for (let classListEntry of selectedCell.classList) {
		if(/board-row-.*/.test(classListEntry)) { cellRowCells = c_BoardRows.get(classListEntry); }
		if(/board-column-.*/.test(classListEntry)) { cellColumnCells = c_BoardColumns.get(classListEntry); }
	}
	let cellsToPaint = new Set([...cellBoxCells, ...cellRowCells, ...cellColumnCells]);
	if (cellsToPaint.has(selectedCell)) { cellsToPaint.delete(selectedCell); }

	for (let boardCell of cellsToPaint) {
		if (paint) {
			boardCell.classList.add("paint-selected");
		} else {
			boardCell.classList.remove("paint-selected");
		}
	}
}

function paintInvalidBoardCells(paint = false) {
	for (let boardCell of g_InvalidBoardCells) {
		if (paint) {
			boardCell.classList.add("paint-invalid");
		} else {
			boardCell.classList.remove("paint-invalid");
		}
	}
	if (!paint) { g_InvalidBoardCells.clear(); }
}

//////////////////////////////////////////////////////////////////////////////////////////

function checkBoard() {
	paintInvalidBoardCells();
	deselectBoardCell();

	for (let boardBox of c_BoardBoxes.values()) {
		checkBoardCellArray(boardBox);
	}
	for (let boardRow of c_BoardRows.values()) {
		checkBoardCellArray(boardRow);
	}
	for (let boardColumn of c_BoardColumns.values()) {
		checkBoardCellArray(boardColumn);
	}

	if (g_InvalidBoardCells.size > 0) {
		paintInvalidBoardCells(true);
	} else {
		showPrompt("Congratulations, you won!", "What would you like to do next?", "victory");
	}
}

function checkBoardCellArray(boardCellsArray) {
	if (g_InvalidBoardCells.size < g_VacantCells) {
		let cellValues = new Array(boardCellsArray.length);

		for (let i in boardCellsArray) {
			let cellValue = parseInt(boardCellsArray[i].innerHTML);
			if (isNaN(cellValue)) {
				cellValue = 0;
				g_InvalidBoardCells.add(boardCellsArray[i]);
			}
			cellValues[i] = cellValue;
		}
		checkSequenceAndMarkInvalidCells(boardCellsArray, cellValues);
	}
}

function checkSequenceAndMarkInvalidCells(boardCellsArray, valuesArray) {
	for (let i = 1; i <= g_TotalGameSize; ++i) {
		let valueLastIndex = valuesArray.lastIndexOf(i);

		for (let j = 0; j < valueLastIndex; ++j) {
			if (valuesArray[j] == i) {
				if (!boardCellsArray[valueLastIndex].disabled) { g_InvalidBoardCells.add(boardCellsArray[valueLastIndex]); }
				if (!boardCellsArray[j].disabled) { g_InvalidBoardCells.add(boardCellsArray[j]); }
			}
		}
	}
}