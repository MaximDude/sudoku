Page: https://maximdude.gitlab.io/sudoku

Fonts from [Google Fonts](https://fonts.google.com).  
Icons from [SVGRepo](https://svgrepo.com).  
Background pattern from [Toptal Subtle Patterns](https://www.toptal.com/designers/subtlepatterns/).